<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Post;

class PostController extends Controller
{

    public function index(Post $postModel)
    {
        $posts = $postModel->getPublishedPosts();
        return view('post.index', ['posts' => $posts]);
    }

    public function unpublished(Post $postModel)
    {
        $posts = $postModel->getUnpublishedPosts();
        return view('post.index', ['posts' => $posts]);
    }

    public function create()
    {
        return view('post.create');
    }

    public  function store(Post $postModel, Request $request)
    {
        //dd($request->all());
        $postModel->create($request->all());
        return redirect()->route('posts');
    }
}
